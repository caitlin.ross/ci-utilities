$erroractionpreference = "stop"

# Keep in sync with the MacOS script.
$git_url = 'https://github.com/Kitware/fletch'
# Initial Windows build; sync with kwiver CI
$git_tag = 'c310f3cee87448a31ffbbb8f24c4bdd828bae071'

git clone "$git_url" fletch\src
git -C fletch/src -c advice.detachedHead=false checkout "$git_tag"

$here = pwd

New-Item -Path "fletch\build" -Type Directory
cd "fletch\build"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -Dfletch_BUILD_INSTALL_PREFIX:PATH="$env:CI_PROJECT_DIR/.gitlab/fletch" `
  -C "$env:CI_PROJECT_DIR/fletch/configure_fletch_kwiver.cmake" `
  "..\src"
ninja
cd ..\..

# create final zip
Compress-Archive -LiteralPath "$env:CI_PROJECT_DIR/.gitlab/fletch" -DestinationPath "$here\fletch-kwiver-$git_tag-windows-x86_64.zip"

# ci-utilities

Some components of our CI setup requires manual work. This project hosts
utilities for rebuilding various bits of our software for us.

## Steps to add a new component

If you need to automate building a package for CI, the following steps will help you add the project and configure this repo's CI to build and upload it. These instructions will use ADIOS2 as an example.

### Add the project directory and build scripts

Add a directory for the component, e.g., `adios2`.

Write scripts that will build the binaries for each platform (Linux, macOS, Windows). You only need scripts for the platforms you need. For instance, for ADIOS2,  we only need to build it for macOS and Windows, since for Linux, we build it in the Docker images, so the adios2 directory contains the `package-macos.sh` and `package-windows.ps1` scripts.

For each platform, you can likely copy the script from another project and edit it as necessary for your project, but the basic steps are:

1. Grab the correct version of the code to be built. If it's a git repo for instance, set variables with the git url, commit, and version, then call the commands to checkout the code
2. Note that all external resources must be hash-verified.
3. Determine the architecture being used and set up some variables for creating the final tar/zip that will be uploaded.
4. Configure, build, and install. Tar/zip the files

### Add builds in .gitlab-ci.yml

Edit `.gitlab-ci.yml` to create builds for the project. Add a comment header where you'll put the information for your project, so we can keep the file organized.

The first section you add will look like (replace `adios2` with your project name):
```yaml
.adios2:
    variables:
        RELEASE_TAG_RE: /^adios2.v/
```

Next you'll create sections for each platform type you need. For ADIOS2 macOS x86\_64 build we have:

```yaml
package:adios2-macos-x86_64:
    extends:
        - .adios2
        - .rules
    stage: ci
    tags:
        - macos-x86_64
        - concurrent
        - shell
        - xcode-14.2
    variables:
        DEVELOPER_DIR: "/Applications/Xcode-14.2.app/Contents/Developer"
        GIT_CLONE_PATH: "$CI_BUILDS_DIR/ci-utilities/$CI_CONCURRENT_ID"
    script:
        - .gitlab/ci/cmake.sh
        - PATH=$PWD/.gitlab/cmake/bin:$PATH
        - cmake --version
        - adios2/package-macos.sh
    interruptible: true
    artifacts:
        expire_in: 1w
        paths:
            - adios2-*.tar.gz
```

The script section calls a script that grabs the CMake binaries, so it can be used in `adios2/package-macos.sh`
The final output that will be uploaded is `adios2-*.tar.gz`.

Next we need to set up the job that will upload the files. Set up the upload job like the following:

```yaml
upload:adios2:
    extends:
        - .upload_job
        - .adios2
        - .rules
    needs:
        - package:adios2-macos-x86_64
        - package:adios2-macos-arm64
        - package:adios2-windows-x86_64
    dependencies:
        - package:adios2-macos-x86_64
        - package:adios2-macos-arm64
        - package:adios2-windows-x86_64
    variables:
        UPLOAD_FILES: adios2-*
```

All of the platforms you're building for need to go under both `needs` and `dependencies`.
The scripts to generate the binaries were written in a way that the name of the  final output starts with `adios2-`.

Finally, add the final job, which creates a release out of the binaries that were built.

```yaml
release:adios2:
    extends:
        - .release_job
        - .adios2
        - .rules
    needs:
        - package:adios2-macos-x86_64
        - package:adios2-macos-arm64
        - package:adios2-windows-x86_64
        - upload:adios2
    dependencies:
        - package:adios2-macos-x86_64
        - package:adios2-macos-arm64
        - package:adios2-windows-x86_64
```

All the platforms need to be listed under `needs` and `dependencies`. The `upload:*` job also should be listed under `needs`.

The last few sections reference `.rules`, `.upload_job`, and `.release_job`. These are all in `.gitlab/components.yml`. None of these need to be edited directly, but `.release_job` references a script, `.gitlab/ci/make-release.sh` that will need to be edited for your project.

In `make-release.sh`, there's a `case` statement where you need to add a section to handle your project. For ADIOS this looks like:

```yaml
    adios2/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls adios2-*-x86_64.tar.gz )"
        macos_arm64_binary="$( ls adios2-*-arm64.tar.gz )"
        windows_x86_64="$( ls adios2-*-windows-x86_64.zip )"
        readonly macos_x86_64_binary
        readonly macos_arm64_binary
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
        ;;
```

First we find the files for each platform. This will match the names you set in the relevant scripts for bulding the project (e.g., `adios2/package-macos.sh`). Then we call the `make_release` function at the top of `make-release.sh`. This uses the [Gitlab Release CLI tool](https://gitlab.com/gitlab-org/release-cli/-/blob/master/docs/index.md#usage) to create a release for each of the platforms.

When you open your MR, you manually trigger the builds on the pipelines page. You only need to test out the builds you're adding/editing.
That will run the build and upload jobs, but the release build won't be triggered until after merge.

### Trigger the release builds

Once your MR has been accepted and merged, you can trigger the release build(s) by creating an unannotated/lightweight tag on the merge commit.
The name of the tag should follow the format: `project/v<version>-YYYYMMDD.N`.
`<version>` is whatever is appropriate for that project.
This will usually be a git tag, but it may be a short commit hash if a specific tag isn't being used.
The date is the current date, and `N` starts at 0 and is incremented for each release that day.
This allows for package-level rebuilds to be made as needed.
For example, for creating the ADIOS2 v2.9.2 release builds, the tag name will look like: `adios2/v2.9.2-20231220.0`.

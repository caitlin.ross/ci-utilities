#!/bin/sh

set -e

. sccache/package-common.sh

echo "Building for x86_64-pc-windows-gnu"
cargo build --no-default-features --features=redis --release --target x86_64-pc-windows-gnu
out="$GIT_CLONE_PATH/sccache-v$version-x86_64-pc-windows-gnu.exe"
cp target/x86_64-pc-windows-gnu/release/sccache.exe "$out"
x86_64-w64-mingw32-strip "$out"

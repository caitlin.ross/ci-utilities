#!/bin/sh

set -e

readonly git_lfs_version="3.5.1"
readonly git_lfs_amd64_sha256="23f6c768e22a33dcbb57d6cb67d318dc0edc2b16ac04b15faa803a74a31e8c42"
readonly git_lfs_arm64_sha256="1570833e5011290dff12a18416580bfed576bc797b7b521122916e09adf4622d"

readonly git_lfs_amd64_url="https://github.com/git-lfs/git-lfs/releases/download/v$git_lfs_version/git-lfs-darwin-amd64-v$git_lfs_version.zip"
readonly git_lfs_arm64_url="https://github.com/git-lfs/git-lfs/releases/download/v$git_lfs_version/git-lfs-darwin-arm64-v$git_lfs_version.zip"

cat > git-lfs.sha256sum <<EOF
$git_lfs_amd64_sha256  git-lfs-darwin-amd64-v$git_lfs_version.zip
$git_lfs_arm64_sha256  git-lfs-darwin-arm64-v$git_lfs_version.zip
EOF
curl -OL "$git_lfs_amd64_url"
curl -OL "$git_lfs_arm64_url"
shasum -a 256 --check git-lfs.sha256sum

mkdir amd64
unzip -d amd64 "git-lfs-darwin-amd64-v$git_lfs_version.zip"
mkdir arm64
unzip -d arm64 "git-lfs-darwin-arm64-v$git_lfs_version.zip"

lipo -create \
    -output "git-lfs-darwin-universal-$git_lfs_version" \
    "amd64/git-lfs-$git_lfs_version/git-lfs" \
    "arm64/git-lfs-$git_lfs_version/git-lfs"

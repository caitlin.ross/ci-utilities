From 282b33d4e0071b865b60e5652d6a239cfe906024 Mon Sep 17 00:00:00 2001
From: Brad King <brad.king@kitware.com>
Date: Wed, 29 Jul 2020 15:38:29 -0400
Subject: [PATCH 2/2] Add an optional check script that runs on the host before
 the executor

Give local runner configurations a chance to check the job before
running it.
---
 commands/multi.go | 26 ++++++++++++++++++++++++++
 common/config.go  |  1 +
 2 files changed, 27 insertions(+)

diff --git a/commands/multi.go b/commands/multi.go
index f964a22f6..5e8cef8c3 100644
--- a/commands/multi.go
+++ b/commands/multi.go
@@ -8,6 +8,7 @@ import (
 	"net/http"
 	"net/http/pprof"
 	"os"
+	"os/exec"
 	"os/signal"
 	"runtime"
 	"sync"
@@ -22,6 +23,7 @@ import (
 	"github.com/urfave/cli"
 
 	"gitlab.com/gitlab-org/gitlab-runner/common"
+	"gitlab.com/gitlab-org/gitlab-runner/common/buildlogger"
 	"gitlab.com/gitlab-org/gitlab-runner/helpers"
 	"gitlab.com/gitlab-org/gitlab-runner/helpers/certificate"
 	prometheus_helper "gitlab.com/gitlab-org/gitlab-runner/helpers/prometheus"
@@ -828,6 +830,30 @@ func (mr *RunCommand) processBuildOnRunner(
 	// to speed up taking the builds
 	mr.requeueRunner(runner, runners)
 
+	if runner.HostCheckScript != "" {
+		var hostCheckCmd *exec.Cmd
+		if runtime.GOOS == "windows" {
+			hostCheckCmd = exec.Command("cmd", "/c", runner.HostCheckScript)
+		} else {
+			hostCheckCmd = exec.Command("sh", "-c", runner.HostCheckScript)
+		}
+		if hostCheckCmd == nil {
+			hostCheckCmdErr := errors.New("failed to construct host check script command")
+			trace.Fail(hostCheckCmdErr, common.JobFailureData{Reason: common.RunnerSystemFailure})
+			return hostCheckCmdErr
+		}
+		hostCheckCmd.Env = append(append(os.Environ(), runner.Environment...),
+		                          build.JobResponse.Variables.StringList()...)
+		hostCheckOut, hostCheckErr := hostCheckCmd.CombinedOutput()
+		if hostCheckErr != nil {
+			var logger = buildlogger.New(trace, build.Log())
+			logger.Println(string(hostCheckOut))
+			trace.Fail(errors.New("host check script failed"),
+			           common.JobFailureData{Reason: common.ScriptFailure, ExitCode: 1})
+			return hostCheckErr
+		}
+	}
+
 	// Process a build
 	return build.Run(mr.getConfig(), trace)
 }
diff --git a/common/config.go b/common/config.go
index 7e1565875..53f145515 100644
--- a/common/config.go
+++ b/common/config.go
@@ -1128,6 +1128,7 @@ type RunnerSettings struct {
 	CloneURL  string `toml:"clone_url,omitempty" json:"clone_url" long:"clone-url" env:"CLONE_URL" description:"Overwrite the default URL used to clone or fetch the git ref"`
 
 	Environment []string `toml:"environment,omitempty" json:"environment,omitempty" long:"env" env:"RUNNER_ENV" description:"Custom environment variables injected to build environment"`
+	HostCheckScript string   `toml:"host_check_script,omitempty" description:"Runner-specific command script executed on host before running job"`
 
 	// DEPRECATED
 	// TODO: Remove in 16.0. For more details read https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29405
-- 
2.43.0


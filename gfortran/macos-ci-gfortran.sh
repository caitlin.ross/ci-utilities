#!/bin/bash

set -e

# Cross compilation is not supported by this script.
target_arch="$( uname -m )"
readonly target_arch

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        arch="x86_64"
        target="$arch-apple-darwin17"
        ;;
    arm64)
        min_macosx_version="11.0"
        arch="aarch64"
        target="$arch-apple-darwin22"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly arch
readonly target

readonly gmp_version="6.3.0"
readonly gmp_filename="gmp-$gmp_version.tar.xz"
readonly gmp_url="https://ftp.gnu.org/gnu/gmp/$gmp_filename"
readonly gmp_sha256sum="a3c2b80201b89e68616f4ad30bc66aee4927c3ce50e33929ca819d5c43538898"

readonly mpfr_version="4.2.1"
readonly mpfr_filename="mpfr-$mpfr_version.tar.xz"
readonly mpfr_url="https://www.mpfr.org/mpfr-$mpfr_version/$mpfr_filename"
readonly mpfr_sha256sum="277807353a6726978996945af13e52829e3abd7a9a5b7fb2793894e18f1fcbb2"

readonly mpc_version="1.3.1"
readonly mpc_filename="mpc-$mpc_version.tar.gz"
readonly mpc_url="https://ftp.gnu.org/gnu/mpc/$mpc_filename"
readonly mpc_sha256sum="ab642492f5cf882b74aa0cb730cd410a81edcdbec895183ce930e706c1c759b8"

readonly isl_version="0.24"
readonly isl_filename="isl-$isl_version.tar.xz"
readonly isl_url="https://libisl.sourceforge.io/$isl_filename"
readonly isl_sha256sum="043105cc544f416b48736fff8caf077fb0663a717d06b1113f16e391ac99ebad"

readonly gcc_version="13.2"
readonly gcc_filename="gcc-$gcc_version-darwin-r0.tar.gz"
readonly gcc_url="https://github.com/iains/gcc-13-branch/archive/refs/tags/$gcc_filename"
readonly gcc_sha256sum="3261b73fabca6b53a57669265a74361930006f4663d6e04b15abf83a040f36e4"

. gfortran/macos-utils.sh

mkdir build
cd build

download_verify_extract gmp "$gmp_url" "$gmp_sha256sum" "$gmp_filename"
download_verify_extract mpfr "$mpfr_url" "$mpfr_sha256sum" "$mpfr_filename"
download_verify_extract mpc "$mpc_url" "$mpc_sha256sum" "$mpc_filename"
download_verify_extract isl "$isl_url" "$isl_sha256sum" "$isl_filename"
download_verify_extract gcc "$gcc_url" "$gcc_sha256sum" "$gcc_filename"

prefix="/opt/gcc"
destination="$( pwd )/gcc-$gcc_version-$arch"

export MACOSX_DEPLOYMENT_TARGET="$min_macosx_version"

echo "Building for $target"

# Set up in-build dependencies.
ln -s ../../gmp/src gcc/src/gmp
ln -s ../../mpfr/src gcc/src/mpfr
ln -s ../../mpc/src gcc/src/mpc
ln -s ../../isl/src gcc/src/isl

build gcc "$prefix" "$destination" \
    --build="$target" \
    --disable-multilib \
    --with-sysroot="$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"

cd ..

tar -C build -cJf "gcc-$gcc_version-macos$min_macosx_version-$arch.tar.xz" "gcc-$gcc_version-$arch"

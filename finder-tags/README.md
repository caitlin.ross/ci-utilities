# Finder tag job

This project is intended to be used to trigger CI jobs on specific macOS
runners we have. This job will perform a CPack action which will cause a
permission request for the `gitlab-runner` binary to appear on the host.

## Using

In order to use this, create a source branch with named with a `finder-`
prefix. There will then be jobs per runner which may be run manually to cause
permission requests to appear.
